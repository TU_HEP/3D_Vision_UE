\chapter{Monkey}
\label{chap:monkey}

\subsection*{Lukas Pechhacker, 1326655}

\section{3D model from the Minolta Laser-Scanner}
This section discusses creating the 3D model from the \textit{Minolta 3D Laser Triangulation Scanner} data with the software \textit{Geomagic Wrap} \parencite{geomagic}.

\subsection{Scanning}
For the scanning process, the object was fixed in a preferred position on a mechanical turntable. The rotation of the turntable between scans was $60^\circ$ and a full revolution was scanned. Those six scans were performed for each pose of the object on the turntable. In total, three different poses of the object  were scanned in $60^\circ$ increments to get a waterproof model. Special attention  was paid to the fact that as far as possible all areas of the object are  picked up by the laser scanner, so that at least once every area of the object  had a direct line of sight to the laser scanner.

\subsection{Processing with \textit{Geomagic Wrap}}
%\begin{wrapfigure}{R}{0.5\textwidth}
%	\centering
%	\includegraphics[width=0.4\textwidth]{img/monkey_merged.png}
%	\caption{Errors after merging}
%	\label{fig:monkey_merged}
%\end{wrapfigure}
The individual scans were arranged correctly by a manual N-point registration to each other, since they did not have the correct orientation to each other due to the scans in three different poses. Subsequently, the total of 18 scans was merged by the function \textit{Merge} and then optimized with the function \textit{Mesh Doctor}. However, the 3D model from the merged scans had some bugs. They had to be eliminated manually as seen in Figure \ref{fig:monkey_merged} on the back of the monkey. The Surface of the 3D model was noisy, had some holes and some wrong boundaries (see Figure \ref{fig:monkey_merged_problem}). The function \textit{Mesh Doctor} optimized and smoothed the surface. The number of triangles dropped from $141328$ to $140464$. The number of triangles was further reduced by the function \textit{Remesh} to $123658$ after the hole-filling process with the \textit{Fill Single} function. The result of the processing can be seen in Figure \ref{fig:monkey_remeshed}. Afterwards, the function \textit{Enhance Mesh for Surfacing} was applied to get an optimized mesh for the subsequent further processing. Some errors on both hands, on the belly and in the mouth can be seen in Figures \ref{fig:monkey_problem_hand_1}, \ref{fig:monkey_problem_hand_2}, \ref{fig:monkey_problem_belly} and \ref{fig:monkey_problem_mouth}. Those errors were eliminated with the function \textit{Defeature} or simply by deleting the mesh on the areas where the errors occured and then filling holes as seen in Figure \ref{fig:monkey_problem_mouth_solved}.

\begin{figure}[htbp!] 
  \begin{minipage}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=.5\linewidth]{monkey_merged.png} 
    \caption{Monkey after merging} 
    \label{fig:monkey_merged}
    \vspace{2ex}
  \end{minipage}%%
  \begin{minipage}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=.5\linewidth]{monkey_merged_belly.png} 
    \caption{Problems after merging} 
    \label{fig:monkey_merged_problem}
    \vspace{2ex}
  \end{minipage} 
  \begin{minipage}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=.5\linewidth]{monkey_remeshed.png} 
    \caption{Monkey after remeshing} 
    \label{fig:monkey_remeshed}
    \vspace{2ex}
  \end{minipage}%% 
  \begin{minipage}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=.5\linewidth]{monkey_problem_hand_1.png} 
    \caption{Problem hand} 
    \label{fig:monkey_problem_hand_1}
    \vspace{2ex}
  \end{minipage} 
   \begin{minipage}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=.5\linewidth]{monkey_problem_hand_2.png} 
    \caption{Problem other hand} 
    \label{fig:monkey_problem_hand_2}
    \vspace{2ex}
  \end{minipage}%%
  \begin{minipage}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=.5\linewidth]{monkey_problem_belly.png} 
    \caption{Problem belly} 
    \label{fig:monkey_problem_belly}
    \vspace{2ex}
  \end{minipage} 
    \begin{minipage}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=.5\linewidth]{monkey_problem_mouth.png} 
    \caption{Problem mouth} 
    \label{fig:monkey_problem_mouth}
    \vspace{2ex}
  \end{minipage}%% 
  \begin{minipage}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=.5\linewidth]{monkey_problem_mouth_hole_filled} 
    \caption{Problem mouth solved} 
    \label{fig:monkey_problem_mouth_solved}
    \vspace{2ex}
  \end{minipage} 
\end{figure}

\begin{figure}[htbp!] 

\end{figure}

\section{Structure from Motion (SfM)}
\subsection{Recording of the pictures}
The pictures were recorded by a \textit{Nikon D4 DSLR} camera with a \textit{Nikkor AF 60mm f/2.8D} lens. The camera-settings are shown in Table \ref{tab:monkey_dslr_settings}. An aperature of F20 was used to get a high depth of field in the taken pictures. A tripod was used to avoid blurred images due to slow shutter speed of 1s. The 117 images were taken from various camera positions around the model and were later used for \gls{SfM}. The camera positions determined by the \textit{PhotoScan} software are shown in Figure \ref{fig:monkey_photoscan_cameras}.

\begin{figure}[htpb!]
  \centering
  \includegraphics[width=.6\linewidth]{monkey_photoscan_cameras} 
  \caption{PhotoScan Camera-positions} 
  \label{fig:monkey_photoscan_cameras}
\end{figure}

\begin{table}
  \centering
  \begin{tabular}{lr}
    \toprule
    \textbf{Parameter} & \textbf{Value} \\\midrule
    Camera Model & Nikon D4\\
    Focal Length & $60mm$\\
    Aperture & $F/20$\\
    ISO & $640$\\
    Exposure Time & $1 sec$\\
    Flash & $off$ \\
    \bottomrule
  \end{tabular}
  \caption{DSLR Camera settings}
  \label{tab:monkey_dslr_settings}   
\end{table}

\subsection{Processing with \textit{Agisoft PhotoScan} and \textit{Geomagic Wrap}}
The first step was to align the pictures in the \textit{Agisoft PhotoScan} Software \parencite{agisoft}. Unused points where remove afterwards. The next step was to build the \textit{Dense Cloud} in the medium setting to get sufficient calculation times. Finally the created mesh was exported in \textit{.wrl}-format to keep the texture and imported in \textit{Geomagic Wrap} \parencite{geomagic}. The hole on the bottom of the monkey was filled by the Software. The export of the textured model from \textit{Geomagic Wrap} \parencite{geomagic} as a \textit{3D-PDF} or \textit{prc}-file always led to an internal error and a crash of the software. A workaround was to use non-textured export-formats like the \textit{.stl}-format. 

\section{Evaluation of the results}
Figure \ref{fig:monkey_final_laser} shows the final 3D model from the \textit{Laser-Scanner} and Figure \ref{fig:monkey_final_sfm} shows the final model from \gls{SfM}. Both images contain embedded 3D models, which can be activated by clicking on the respective image. This requires a \textit{3D-PDF}-capable PDF reader.

As seen in the figures, the 3D model of the \textit{Laser-Scanner} has a much smoother surface than the 3D model of \gls{SfM}. Possible reasons are either too much of smoothing by the \textit{Mesh Doctor} or the \textit{remeshing}. The \gls{SfM} model has only about half of the triangles as the \textit{Laser-Scanner} model. Except for filling the hole on the underside of the \gls{SfM} 3D model, the \gls{SfM} model did not need to be post-processed with \textit{Geomagic Wrap}. However, the computing time for creating the \textit{Dense Cloud} was a few hours. Both methods, however, yielded to respectable results.

To investigate the accuracy of the \textit{Minolta 3D laser scanner} and the \gls{SfM}, representative lengths of the physical object were measured with a caliper gauge and then compared to the 3D models. The measured dimensions of the \textit{Laser-Scanner} are more accurate, than the dimensions of the \gls{SfM}. Errors can occur due to inaccurate length measurements in the 3D software or on the physical object. The number of triangles in the model from \gls{SfM} is roughly about half of the number of triangles from the Laser-Scanner. The volumes of the 3D models and all the other attributes can be found in Table \ref{tab:monkey_evaluation}. 

\begin{table}
  \centering
  \begin{tabular}{lccr}
    \toprule
    \textbf{Attribute} & \textbf{Physical object} & \textbf{Laser-Scanner} & \textbf{SfM}\\\midrule	
    Width of the mouth & $14.3mm$ & $14.6mm$ & $14.7mm$ \\
    Overall height & $84.2mm$ & $85.4mm$ & $86mm$ \\ 
    Number of triangles & $-$ & $121 872$ & $68 410$ \\
    Volume & $-$ & $83826 mm^{3}$ & $832784 mm^{3}$  \\
    \bottomrule
  \end{tabular}
  \caption{Evaluation of the representative dimensions}
  \label{tab:monkey_evaluation}   
\end{table}

%\movieref[3Dcalculate]{monkey_3d_sfm}{Calculate 3D settings SFM} 
%\movieref[3Dcalculate]{monkey_3d_laser}{Calculate 3D settings Laser}  

\begin{figure}[htpb!]
 \centering
 \includemedia[
    width=0.5\linewidth,height=0.5\linewidth,
    width=0.5\linewidth,height=0.5\linewidth,
    activate=pageopen,
    3Dtoolbar,
    3Dmenu,
    3Droo=212.8142149371074,
    3Dcoo=-4.744243621826172 0.6770458221435547 -4.099627494812012,
    3Dc2c=-0.10199659317731857 0.0432366244494915 0.9938446879386902,
    3Droll=-117.12146109473737,
    3Dlights=Headlamp,
  ]{\includegraphics{monkey_final_laser.png}}{monkey_laser_geomagic_prc.prc}
  \caption{3D model of the monkey from the Laser-Scanner}
  \label{fig:monkey_final_laser}
\end{figure}

\begin{figure}[htpb!]
  \centering
  \includemedia[
    width=0.5\linewidth,height=0.5\linewidth,
    width=0.5\linewidth,height=0.5\linewidth,
    activate=pageopen,
    3Dtoolbar,
    3Dmenu,
    3Droo=0.21306901042112425,
    3Dcoo=-0.02950548753142357 0.0034353695809841156 -0.3127511441707611,
    3Dc2c=0.2710888385772705 0.03182898461818695 -0.9620279669761658,
    3Droll=85.15541762807982,
    3Dlights=Headlamp,
  ]{\includegraphics{monkey_final_sfm.png}}{monkey_sfm_photoscan_u3d.u3d}
  \caption{3D model of the monkey from SfM}
  \label{fig:monkey_final_sfm}
\end{figure}
